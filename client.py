import asyncio
import random
import string
import websockets


async def hello():
    uri = "ws://localhost:8765"
    async with websockets.connect(uri) as websocket:
        name = ''.join(random.choices(string.ascii_uppercase + string.digits, k=5))

        await websocket.send(name)
        print(f"> {name}")

        greeting = await websocket.recv()
        print(f"< {greeting}")


async def test():
    for x in inputs:
        print(x)


inputs = ["nothing"]

asyncio.get_event_loop().run_until_complete(asyncio.wait([hello(), test()]))
asyncio.get_event_loop().run_forever()
