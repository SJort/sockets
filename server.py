"""
https://websockets.readthedocs.io/en/stable/intro.html
"""
import asyncio
import websockets


async def hello(websocket, path):
    name = await websocket.recv()
    print(f"< {name}")

    greeting = f"Hello {name}!"

    await websocket.send(greeting)
    print(f"> {greeting}")


async def test():
    for x in inputs:
        print(x)


inputs = ["nothing"]

start_server = websockets.serve(hello, "localhost", 8765)

loop = asyncio.get_event_loop()
loop.run_until_complete(asyncio.wait([start_server, test()]))
loop.run_forever()
