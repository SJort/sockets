"""
https://dev.to/welldone2094/async-programming-in-python-with-asyncio-12dl
https://www.aeracode.org/2018/02/19/python-async-simplified/
async = concurrent = not parallel = constantly switching between tasks
await in async function allows loop to switch tasks

async def ....
calling a async function immediately returns a coroutine object (='promise'
to get result: give coroutine to event loop
await = give coroutine to event loop and return result
"""
import asyncio
import time

time.sleep(1)
async def get_chat_id(name):
    await asyncio.sleep(3)
    return "chat-%s" % name

async def main():
    result = await get_chat_id("django")